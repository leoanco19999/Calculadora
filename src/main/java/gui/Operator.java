package gui;

import java.util.*;
import java.util.logging.Logger;

final class Operator {
    private Operator() {} // utility class w/ only static methods doesn't require instances

    private static final Logger LOGGER = Logger.getLogger( Operator.class.getName());

    static String infixToPostfix(String infix) {
        StringBuilder postfix = new StringBuilder();  // initializing empty String for postfix
        Stack<Character> stack = new Stack<>();         // initializing empty stack for the operators

        LOGGER.info("Starting conversion infix to postfix...");
        for (int i = 0; i < infix.length(); ++i) {
            char c = infix.charAt(i);
            if(Character.toString(c).matches("[ ]")) // skip the loop if char is blank
                continue;

            if (Character.isLetterOrDigit(c) || Character.toString(c).matches("[.]")) {  // If the scanned character
                postfix.append(c);   // is an operand or floating point, add it to output.
                if(i+1 < infix.length()) {
                    if(!Character.isDigit(infix.charAt(i+1)) && !Character.toString(infix.charAt(i+1)).matches("[.]")) {
                            postfix.append(" "); // Space out different numbers
                    }
                }
            } else if (c == '(') // If the scanned character is an '(', push it to the stack.
                stack.push(c);
            else if (c == ')') { //  If the scanned character is an ')', pop and output from the stack
                while (!stack.isEmpty() && stack.peek() != '(') // until an '(' is encountered.
                    postfix.append(stack.pop());

                if (!stack.isEmpty() && stack.peek() != '(') {
                    LOGGER.info("Found invalid parenthesis input.");
                    return "Invalid Expression"; // invalid expression
                }
                else
                    stack.pop();
            } else { // an operator is encountered
                while (!stack.isEmpty() && Precedency(c) <= Precedency(stack.peek()))
                    postfix.append(stack.pop());

                stack.push(c);
            }
        }
        while (!stack.isEmpty())
            postfix.append(stack.pop()); // pop all the operators from the stack

        LOGGER.info("Postfix: " + postfix);
        return postfix.toString();
    }
    private static int Precedency(char ch) {
        switch (ch) {
            case '+':
            case '-':
                return 1;

            case '*':
            case '/':
                return 2;

            case '^':
                return 3;
        }
        return -1;
    }
    static double evaluatePostfix(String postfix) {
        Stack<Double> s = new Stack<>();
        LOGGER.info("Starting postfix evaluation...");
        boolean isMultiDigit = false;               // check for numbers > 9
        boolean isDecimal = false;
        for (int i = 0; i < postfix.length(); i++) {
            char curChar = postfix.charAt(i);
            if (Character.toString(curChar).matches("[ .]")) // skip the loop if char is blank
                continue;

            if (Character.isLetterOrDigit(curChar)) {
                if (i!=0) {
                    if (Character.isDigit(postfix.charAt(i - 1)) && !isDecimal) {
                        LOGGER.info("Evaluating multi character digit..."); // found a number > 9
                        double multiDigit = s.pop();                  // remove last number from the stack
                        multiDigit = multiDigit * 10 + ((double) curChar - '0'); // make so, e.g., 10 is not [1.0][0.0] but [10]
                        s.push(multiDigit);                           // push [10] to the stack
                        LOGGER.info(multiDigit + " placed into the stack");
                        isMultiDigit = true;
                    }
                    if(Character.toString(postfix.charAt(i-1)).matches("[.]")){
                        double decimalCase = 1;
                        LOGGER.info("Evaluating decimal  digit...");
                        double decimalDigit = s.pop() + ((double) curChar -'0')/(Math.pow(10,decimalCase));
                        s.push(decimalDigit);
                        isDecimal = true;
                        decimalCase++;
                    }
                }
                if (!isMultiDigit && !isDecimal) {                           // if the last if was true, ignore this one
                    s.push((double) curChar - '0');
                    LOGGER.info(curChar + " placed into the stack");
                }

                if (i + 1 < postfix.length()) {
                    char nextChar = postfix.charAt(i + 1);
                    if (!Character.isDigit(nextChar)) {
                        isMultiDigit = false;               // reset the multiDigit check
                        isDecimal = false;
                    }
                }
            }
            else {
                LOGGER.info("Operator found...");      // if curChar is an operation
                double val1 = s.pop();                      // evaluate top two numbers on stack
                double val2 = s.pop();
                LOGGER.info("Solving " + val2 + " " + curChar + " " + val1);
                switch (curChar) {
                    case '+':
                        s.push(val2 + val1);
                        LOGGER.info("Result: " + s.peek());
                        break;

                    case '-':
                        s.push(val2 - val1);
                        LOGGER.info("Result: " + s.peek());
                        break;

                    case '/':
                        s.push(val2 / val1);
                        LOGGER.info("Result: " + s.peek());
                        break;

                    case '*':
                        s.push(val2 * val1);
                        LOGGER.info("Result: " + s.peek());
                        break;

                    case '^':
                        s.push(Math.pow(val2, val1));
                        LOGGER.info("Result: " + s.peek());
                        break;

                    }
                }
            }
        return s.pop();
    }
}
