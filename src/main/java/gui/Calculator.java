package gui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Calculator extends Application {

    public static void main(String[] args) { launch(args); }

    private static final Logger LOGGER = Logger.getLogger( Calculator.class.getName());

    @Override
    public void start(Stage primaryStage) {
        try{
            Parent root = FXMLLoader.load(getClass().getResource("/fxml/GUICalculator.fxml"));
            primaryStage.setTitle("Calculator");
            Scene scene = new Scene(root, 250, 275);
            primaryStage.setScene(scene);
            primaryStage.show();
        }catch(java.io.IOException error) {
            LOGGER.log(Level.SEVERE, error.toString(), error);
        }
    }
}