package gui;

import jdk.internal.util.xml.impl.Input;

import java.util.logging.Logger;

public class InputValidator {
    private static final Logger LOGGER = Logger.getLogger( InputValidator.class.getName());

    public InputValidator(){
    }

    public boolean isInputValid(String input){
        LOGGER.info("Validating Input...");

        return input.matches("^\\s*([-+]?)(\\(?[+-]?\\d+((\\.)?(\\d)*)\\)?)(?:\\s*\\s*([-+*\\/^])\\s*(([-+]?)([(]?)" +
                "(?:\\s*[-+])?\\d+((\\.)?(\\d)*))\\s*([)]?)\\s*)+$");
    }
}
// ^\\s*([-+]?)(\\(?[+-]?\\d+((\\.)?(\\d)*)\\)?)(?:\\s*\\s*([-+*\\/^])\\s*(([-+]?)([(]?)(?:\\s*[-+])?\\d+((\\.)?(\\d)*))\\s*([)]?)\\s*)+$