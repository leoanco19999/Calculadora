package gui;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import java.util.logging.Logger;


public class FXMLController {

    private static final Logger LOGGER = Logger.getLogger( Calculator.class.getName());

    @FXML
    private TextArea displayField;

    @FXML
    void addToVisor(ActionEvent event){
        Button btnNumber = (Button)event.getSource();
        if((btnNumber.getText().equals("+") || btnNumber.getText().equals("-")) && (displayField.getText().isEmpty()
                || displayField.getText().endsWith("(") || displayField.getText().endsWith("+") ||
                displayField.getText().endsWith("-"))) {
            displayField.setText(displayField.getText() + '0' + btnNumber.getText());
        }
        else
            displayField.setText(displayField.getText() + btnNumber.getText());
    }

    @FXML
    void handleBtnResult(){
        InputValidator validator = new InputValidator();
        String infix = displayField.getText();             // receives, e.g., 5 + 4 / 2
        if(validator.isInputValid(infix)){
            String postfix = Operator.infixToPostfix(infix);
            double result = Operator.evaluatePostfix(postfix);
            displayField.setText(Double.toString(result));
        }
        else{
            displayField.setText("Error: Invalid Input");
            LOGGER.info("Invalid input");
        }
    }

    @FXML
    void clearScreen(){
        displayField.setText("");
    }
    @FXML
    void erase(){
        if(!(displayField.getLength() < 1))
            displayField.setText(displayField.getText().substring(0,displayField.getLength()-1));
        else
            LOGGER.info("Nothing to erase");
    }
}

