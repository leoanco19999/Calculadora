#Todo
- ~~Transport Calculator from AWT to JavaFX~~
    - ~~Create the visual part of the calculator~~
    - ~~Apply functionalities for the buttons~~
- Make the Calculator have a better visual design
- ~~Validate input~~
- format input to remove redundancies

#ToFix
- ~~potency case at "evaluatePostfix(String postfix)" in Operator class~~
- ~~make "evaluatePostfix(String postfix)" work with digits bigger than 9~~
- ~~convert the Java project to a Maven project~~
- ~~evaluation of float digits~~
- ~~evaluation with there are more operators than operands~~
- ~~validate input still approves invalid inputs~~